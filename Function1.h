

#ifndef FUNCTION1_H
#define FUNCTION1_H

#include <iostream>
#include <cstring>
#include <cstdlib>

using namespace std;

/* The DIGIT namespace to differentiate Function1 from Function2 */
namespace digit{
        /* Our function to test for valid DIGIT strings */
        bool isValid(string input, char& exception);
}


#endif
