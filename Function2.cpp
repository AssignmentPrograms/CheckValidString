/*
 * File Name: Function2.cpp
 * Author: Madhav Gautam
 * Student ID: P428j884
 * Assignment Number: Exam 2
 */

#include "Function2.h"

/* The ALPHA namespace to differentiate Function1 from Function2 */
namespace alpha{

        /* A helper function to test a single character of our string */
        bool testChar(char input, char& exception){
                try{
                        if(isalpha(input)){
                                return true;
                        }
                        else if(isdigit(input)){
                                return false;
                        }
                        else{
                                throw input;
                        }
                }
                catch(char e){
                        exception = e;
                        return false;
                }
        }

        /* A function to determine if string contents are valid for the namespace */
        bool isValid(string input, char& exception){
                /* Reset our exception flag ('0' indicates no exception)
                 *which also stores the invalid character (if any).
                 */
                exception = '0';

                int string_len = input.length();

                /* The return value, indicating success or failure
                 * Success = Valid string (Contains Only Alpha Chars)
                 * Failure = Invalid string (Contains Other Chars)
                 */
                bool return_val = true;

                /* Test each character in the string */
                for(int i = 0; i < string_len; i++){
                        if(!testChar(input[i], exception)){
                                return_val = false;
                        }
                }
                return return_val;
        }
}
