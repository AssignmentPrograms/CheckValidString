

#ifndef FUNCTION2_H
#define FUNCTION2_h

#include <iostream>
#include <cstring>
#include <cstdlib>

using namespace std;

/* The ALPHA namespace to differentiate Function1 from Function2 */
namespace alpha{
        /* Our function to test for valid ALPHA strings */
        bool isValid(string input, char& exception);
}


#endif
