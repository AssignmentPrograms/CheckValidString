/*
 * File Name: FunctionDemo.cpp
 * Author: Madhav Gautam
 * Student ID: P428j884
 * Assignment Number: Exam 2
 */

#include <iostream>
#include <string>
#include "Function1.h"
#include "Function2.h"

/* The namespaces we wish to use, including std for cout names */
using namespace std;

/* alpha and digit are self defined namespaces that contains
 *string validation checks for the corresponding type (alpha/digit)
 */
using namespace alpha;
using namespace digit;

int main(){
        /* Values to keep track of number of corresponding strings */
        int digit_num_strings = 3;
        int alpha_num_strings = 3;

        /* Arrays of our string values to test, for convinience */
        /* NOTE: Adding string values here will require an update to
         *the '..._num_strings' variable, so that the for loop knows.
         */
        string digit_strings[3] = {"123456", "ab12AB", "ab12A*"};
        string alpha_strings[3] = {"abCDef", "ab12AB", "ab12A*"};

        /* '0' serves as a flag indicating no exception */
        char exception = '0';

        /* The return value, indicating success or failure
         * Success = Valid string (Contains Only Alpha Chars)
         * Failure = Invalid string (Contains Other Chars)
         */
        bool return_val;

        /* Loops to cycle through each string in array */
        /* Try/Catch is within to catch any exceptions thrown from
         *the 'isValid' Function, to be handled in main() (as requested)
         */
        for(int i = 0; i < digit_num_strings; i++){
                try{
                        cout << "digit : " << digit_strings[i] << " > ";
                        return_val = digit::isValid(digit_strings[i], exception);
                        if(exception != '0'){
                                throw exception;
                        }
                        else if(return_val){
                                cout << "TRUE!" << endl;
                        }
                        else{
                                cout << "FALSE!" << endl;
                        }
                }
                catch(char e){
                        cout << "EXCEPTION WAS THROWN: " << e
                        << " is neither an alpha nor a digit character.\n";
                }
        }
        for(int i = 0; i < alpha_num_strings; i++){
                try{
                        cout << "alpha : " << alpha_strings[i] << " > ";
                        return_val = alpha::isValid(alpha_strings[i], exception);
                        if(exception != '0'){
                                throw exception;
                        }
                        else if(return_val){
                                cout << "TRUE!" << endl;
                        }
                        else{
                                cout << "FALSE!" << endl;
                        }
                }
                catch(char e){
                        cout << "EXCEPTION WAS THROWN: " << e
                        << " is neither an alpha nor a digit character.\n";
                }
        }
}
